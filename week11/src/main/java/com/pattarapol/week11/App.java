package com.pattarapol.week11;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Bat bat1 = new Bat("BatBat");
        bat1.eat();
        bat1.sleep();
        bat1.takeoff();
        bat1.fly();
        bat1.landing();
        System.out.println();
        Fish fish1 = new Fish("FishFish");
        fish1.eat();
        fish1.swim();
        fish1.sleep();
        System.out.println();
        Plane plane1 = new Plane("NokAir", "NokAir Engine");
        plane1.takeoff();
        plane1.fly();
        plane1.landing();
        System.out.println();
        Crocodile crocodile1 = new Crocodile("Spite");
        crocodile1.craw();
        crocodile1.eat();
        crocodile1.swim();
        crocodile1.sleep();
        System.out.println();
        Snake snake1 = new Snake("Eid");
        snake1.craw();
        snake1.eat();
        snake1.sleep();
        System.out.println();
        Submarine submarine1 = new Submarine("Dum", "Dum Engine");
        submarine1.swim();
        System.out.println();
        Bird bird1 = new Bird("Jib");
        bird1.eat();
        bird1.takeoff();
        bird1.fly();
        bird1.landing();
        bird1.sleep();
        bird1.walk();
        bird1.run();
        System.out.println();
        Cat cat1 = new Cat("Maree");
        cat1.eat();
        cat1.walk();
        cat1.run();
        cat1.sleep();
        System.out.println();
        Dog dog1 = new Dog("Boa");
        dog1.eat();
        dog1.walk();
        dog1.run();
        dog1.sleep();
        System.out.println();
        Human human1 = new Human("Tee");
        human1.eat();
        human1.walk();
        human1.run();
        human1.sleep();
        System.out.println();
        Rat rat1 = new Rat("Rattatui");
        rat1.eat();
        rat1.walk();
        rat1.run();
        rat1.sleep();
        System.out.println();


        Flyable flyablesobjects[] = {bat1,plane1,bird1};
        for(int i=0; i < flyablesobjects.length;i++){
            flyablesobjects[i].takeoff();
            flyablesobjects[i].fly();
            flyablesobjects[i].landing();
        }
        System.out.println();
        Crawable crawableobjects[] = {crocodile1,snake1};
        for(int i=0; i < crawableobjects.length;i++){
            crawableobjects[i].craw();
        }
        System.out.println();
        Swimable swimableobjects[] = {fish1,submarine1,crocodile1};
        for(int i=0; i < swimableobjects.length;i++){
            swimableobjects[i].swim();
        }
        System.out.println();
        Walkable walkableobjects[] = {human1,cat1,dog1,rat1,bird1};
        for(int i=0; i < walkableobjects.length;i++){
            walkableobjects[i].walk();
            walkableobjects[i].run();

        }


    }
}
