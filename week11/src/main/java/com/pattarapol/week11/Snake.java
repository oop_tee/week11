package com.pattarapol.week11;

public class Snake extends Animal implements Crawable {

    public Snake(String name) {
        super("snake", 0);
    }

    @Override
    public void craw() {
        System.out.println(this + " craw.");
        
    }

    @Override
    public void eat() {
        System.out.println(this + " eat.");
        
    }

    @Override
    public void sleep() {
        System.out.println(this + " sleep.");
        
    }
    @Override
    public String toString() {
        return "snake(" +this.getName() + ")";
    }
    
}
